{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Primes
  ( primes
  ) where

{-
>>> primes 100 :: [Int]
[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97]
-}
primes :: Integral a => a -> [a]
primes n = 2 : primes' 3 6 [1, 5]
  where
    filterN = takeWhile (<= n)
    primes' p m qs@(_:q:_)
      | m < n = p : primes' q (m * q) (minus (concat (take (fromIntegral q) $ iterate(map (+ m)) qs)) (map (*q) qs))
      | otherwise = p : naiveFilter (filterN qs)
    naiveFilter ps@(_:p:_)
      | p * p > n = tail ps
      | otherwise = p : naiveFilter (minus ps (filterN $ map (*p) ps))
    minus x@(a:as) y@(b:bs) = case compare a b of
      LT -> a : minus as y
      EQ -> minus as bs
      GT -> minus x bs
    minus [] _  = []
    minus as [] = as
